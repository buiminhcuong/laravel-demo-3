import React from 'react';
import TestComponentInput from './TestComponentInput';
import TestComponentLabel from './TestComponentLabel';

class TestComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value
        };
    };

    handleChange(e) {
        this.setState( {
            ...this.state,
            value: e.target.value
        });
    };

    render() {
        return(<div>
            <TestComponentInput
                onChange={(text) => this.handleChange(text)} />
            <TestComponentLabel
                value={this.state.value}/>
        </div>);
    }
}

export default TestComponent;
