import React from 'react';

class TestComponentInput extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (<div>Input:
            <input type="text"
                onChange={this.props.onChange}/>
        </div>);
    }
}

export default TestComponentInput;
