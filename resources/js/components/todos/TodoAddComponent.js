import React from 'react';

class TodoAddComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var inputRef = React.createRef();
        return (<div>
                Add todo: <input type="text" ref={inputRef} />
                <input type="button" value="Add todo"
                    onClick={() => {
                        this.props.onAdd(inputRef.current.value);
                        inputRef.current.value = '';

                    }}/>
            </div>);
    }
}

export default TodoAddComponent;
