import React from 'react';

class TodoListComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var items = this.props.items.map(item => {
            return (<li key={item.id}>{item.text}</li>);
        })
        return (<div>
                <ul>{items}</ul>
            </div>);
    }
}

export default TodoListComponent;
