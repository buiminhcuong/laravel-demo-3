import React from 'react';

class TodoFilterComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (<div>
                Filter: <input value={this.props.filter}
                    onChange={this.props.onFilterChange}/>
            </div>);
    }
}

export default TodoFilterComponent;
