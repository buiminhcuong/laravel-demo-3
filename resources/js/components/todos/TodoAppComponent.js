import React from 'react';
import TodoAddComponent from './TodoAddComponent';
import TodoFilterComponent from './TodoFilterComponent';
import TodoListComponent from './TodoListComponent';

class TodoAppComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: '',
            lastId: 2,
            todos: [
                {
                    id: 1,
                    text: 'todo item 1'
                },
                {
                    id: 2,
                    text: 'todo item 2'
                }
            ]
        }
    }

    handleFilterChange(e) {
        this.setState({
            ...this.state,
            filter: e.target.value
        });
    }

    getTodos() {
       return this.state.todos.filter((item) => {
            var filterText = this.state.filter;
                 return ! filterText || item.text.includes(filterText)
            })
    }

    handleAdd(text) {
        var newState = {...this.state};
        var newId = newState.lastId + 1;
        newState.todos.push(
            {
                id: newId,
                text
            }
        );
        newState.lastId = newId;
        this.setState(newState);
    }

    render() {
        return (<div>
                Todo app component
                <TodoFilterComponent filter={this.state.filter}
                    onFilterChange={(e) => this.handleFilterChange(e)}/>
                <TodoAddComponent
                    onAdd={(text) => this.handleAdd(text) }/>
                <TodoListComponent items={this.getTodos()}/>
            </div>);
    }
}

export default TodoAppComponent;
