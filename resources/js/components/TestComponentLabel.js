import React from 'react';

class TestComponentLabel extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (<div>Content:
            {this.props.value}
        </div>);
    }
}

export default TestComponentLabel;
