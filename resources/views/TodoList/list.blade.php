@extends('layouts.mainlayout')

@section('header-text')
    @parent - this is todolist view
    <h1>{{$mytitle}}</h1>
@endsection

@section('maincontent')

<table>
    <tr>
        <td>Title</td>
        <td>Description</td>
@foreach ($helloWorld as $todolist)
        <tr>
            <td>{{$todolist->title}}</td>
            <td>{{$todolist->description}}</td>
        </tr>
@endforeach
</table>
@endsection
