<html>
    <head>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <style>
            body {
                margin: 0;
            }

            .header {
                background-color: blue;
                color: white;
                padding: 40px;
                font-size: 40px;
                font-weight: bolder;
            }

            .footer {
                background-color: blue;
            }
        </style>
    </head>
    <body>
        <div class="header">
            @section('header-text')
                Hello CNTT1
            @show
        </div>
        @section('maincontent')

        @show
    </body>
</html>
